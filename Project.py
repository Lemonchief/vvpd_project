from math import sqrt


def cg_distance(coordinates):
    first_x_center = (coordinates[0] + coordinates[2]) / 2
    second_x_center = (coordinates[4] + coordinates[6]) / 2
    first_y_center = (coordinates[1] + coordinates[3]) / 2
    second_y_center = (coordinates[5] + coordinates[7]) / 2
    result = sqrt((first_x_center - second_x_center)**2 +
                  (first_y_center - second_y_center)**2)
    return result


def corner_distance(coordinates):
    left_up_corners = sqrt((coordinates[0] - coordinates[4])**2 +
                           (coordinates[1] - coordinates[5])**2)
    right_down_corners = sqrt((coordinates[2] - coordinates[6])**2 +
                              (coordinates[3] - coordinates[7])**2)
    result = left_up_corners + right_down_corners
    return result


def main():
    coordinates = None
    while True:
        try:
            coordinates = list(map(int, input('Введите координаты верхних левых '
                                              'и нижних правых углов двух '
                                              'прямоугольников(Всего их 8).').split()))
        except ValueError:
            print('ОШИБКА! Неверный тип данных.')
            continue

        if len(coordinates) != 8:
            print('ОШИБКА! Неверное количество введённых данных.')
            continue
        break

    while True:
        input_command = input('Введите команду из следующего списка(одну):\n'
                              'A-Найти расстояние между центрами тяжести'
                              ' двух прямоугольников.\n'
                              'B-Найти сумму расстояний между верхними левыми'
                              ' и нижними правыми углами двух прямоугольников.\n'
                              'END-завершениe работы'
                              ' программы.\n').upper().strip()
        commands = {'A': cg_distance, 'B': corner_distance}

        if input_command == 'END':
            break
        try:
            res = commands[input_command](coordinates)
        except KeyError:
            print('ОШИБКА: некорректный ввод команды!')
            continue

        if input_command == 'A':
            print(f'Расстояние между центрами тяжести прямоугольников равно {res}.')
        elif input_command == 'B':
            print(f'Сумма расстояний между верхними левыми и '
                  f'нижними правыми углами двух прямоугольников '
                  f'равна {res}.')



if __name__ == '__main__':
    main()
